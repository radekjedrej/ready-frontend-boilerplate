import FontFaceObserver from 'fontfaceobserver';

function fontLoading() {
    let primaryFont = new FontFaceObserver('Lato', {
        weight: 300
    });

    Promise.all([
        primaryFont.load(null, 3000)
    ]).then(() => {
        document.documentElement.classList.add('webfont-1-active');

        sessionStorage.font1Active = true;
    }).catch(() => {
        document.documentElement.classList.add('webfont-inactive');
    });
}

export default fontLoading;
