const settings = require('./project-settings'),
    myPaths = require('./project-paths'),
    glob = require('glob');

const PhpManifestPlugin = require('webpack-php-manifest'),
    FaviconsWebpackPlugin = require('favicons-webpack-plugin'),
    PurgecssPlugin = require('purgecss-webpack-plugin'),
    CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    plugins: [
        new PhpManifestPlugin({
            devServer: process.env.WEBPACK_DEV_SERVER,
            phpClassName: 'webpackAssets'
        }),

        // Generates favicons from a single image file
        new FaviconsWebpackPlugin({
            logo: `${myPaths.img}/favicon/favicon.png`,
            background: settings.colors.primary,
            title: settings.title,
            emitStats: true,
            statsFilename: 'favicons-data.json',
            // This will NOT inject the markup anywhere.
            // Currently I can't find an easy way to do this.
            inject: false
        }),


        // Removing unused CSS
        new PurgecssPlugin({
            // Give paths to parse for rules. These should be absolute!
            // By using `glob` we can check multiple files
            paths: glob.sync(
                `${myPaths.phpPartials}/**/*.**`
            )
        }),


        new CopyWebpackPlugin([
            { from: `${myPaths.root}/index.php`, to: myPaths.dist },
            { from: myPaths.phpPartials, to: `${myPaths.dist}/partials` },
            { from: `${myPaths.root}/lib`, to: `${myPaths.dist}/lib` }
        ])
    ]
};
