const path = require('path');

module.exports = {
    root: path.resolve(__dirname, '../'),
    config: path.resolve(__dirname),
    partials: path.resolve(__dirname, '../partials'),
    phpPartials: path.resolve(__dirname, '../partials'),
    assets: path.resolve(__dirname, '../assets'),
    scss: path.resolve(__dirname, '../assets/scss'),
    js: path.resolve(__dirname, '../assets/js'),
    img: path.resolve(__dirname, '../assets/img'),
    dist: path.resolve(__dirname, '../dist'),
    indexHtml: path.resolve(__dirname, '../index.html'),
};
