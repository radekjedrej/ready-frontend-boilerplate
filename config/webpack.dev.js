const webpack = require('webpack'),
    merge = require('webpack-merge'),
    common = require('./webpack.common.js');

const myPaths = require('./project-paths'),
    settings = require('./project-settings');


// Plugins
const StyleLintPlugin = require('stylelint-webpack-plugin');


module.exports = merge(common, {
    entry: {
        main: [
            `${myPaths.js}/main.js`,

            // By having .scss file as an entry it will
            // be compiled into it's own `.css` file.
            `${myPaths.scss}/main.scss`
        ]
    },

    mode: 'development',

    output: {
        path: myPaths.dist,
        filename: 'main.js',
        // publicPath: '/'
    },

    devServer: {
        // https://stackoverflow.com/questions/35412137/how-to-get-access-to-webpack-dev-server-from-devices-in-local-network
        // To get your computers IP on the network go to `system-prefereces/network-utility`
        // and under wifi you will see your IP address for the network.
        // contentBase: 'assets/dist',
        host: '0.0.0.0', // This lets the server listen for requests from the network, not just localhost
        port: 1234, // Port the site will be served using

        contentBase: myPaths.root,
        inline: true,
        hot: true, // Turns on hot module reloading
        open: true, // Opens new tab in browser
        overlay: true, // Shows fullscreen errors in browser when there is a problem

        proxy: {
            '/': {
                target: 'http://ready-febp.local:81',
                changeOrigin: true,
            }
        }
    },

    module: {
        rules: [
            {
                test: /\.scss$/,
                use: [
                    'style-loader', // creates style nodes from JS strings
                    'css-loader', // translates CSS into CommonJS
                    'sass-loader' // compiles Sass to CSS, using Node Sass by default
                ]
            },
            // This setup is needed when you have 2 loaders
            // and also need options.
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [{
                    loader: 'babel-loader',
                }]
            }
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),

        new StyleLintPlugin({
            // Prevents errors from appearing in the browser
            emitErrors: false
        })
    ]
});
